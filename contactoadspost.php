<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Contacto</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
		<?php include('contenido/header.php'); ?>
		<?php include('contenido/analytics.php'); ?>
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Contáctanos</h2>

				</div>
			</div>

			<div class="contact-box">
				<div class="container">
					<div class="row">
						
						<div class="col-md-12" align="center">

							 
						<div class="container">
							<div class="col-md-12" >
								<?php include('jotformpost.php'); ?>
							</div>
						</div>
					</div>
					
					<div class="col-md-3">
						<div class="contact-information">
							<h3>Información de Contacto</h3>
							<ul class="contact-information-list">
								<li><span><i class="fa fa-home"></i>Periférico Paseo de la República #601</span> <span>Col. Los Manantiales. </span> <span> Morelia, Michoacán</span></li>
								<li><span><i class="fa fa-phone"></i>(443) 340 0160</span></li>
								<li><i class="fa fa-phone"></i><span>Taller de Servicio <strong>Ext. 100</strong></span><br>
									<i class="fa fa-phone"></i><span>Gerencia de Servicio<strong>Ext. 102</strong></span><br>
									<i class="fa fa-phone"></i><span>Refacciones <strong>Ext.403</strong></span><br>
									<i class="fa fa-phone"></i><span>Ventas <strong>Ext. 201</strong></span><br>                                    
									<i class="fa fa-phone"></i><span>Seminuevos <strong>Ext. 300</strong></span><br>
									<i class="fa fa-phone"></i><span>Gerencia Seminuevos<strong>Ext. 301</strong></span><br>
									<i class="fa fa-phone"></i><span>Contabilidad <strong>Ext. 401</strong></span><br>  
									<i class="fa fa-phone"></i><span>Financiamiento <strong>Ext. 205</strong></span><br>                            
								</li>
								<h3>Whatsapp</h3>
								<li>
									<span><i class="fa fa-whatsapp"></i>
										<strong>   
											Ventas   y   Postventa <br> 
											<a href="https://api.whatsapp.com/send?phone=524425923862 &text=Hola,%20Quiero%20más%20información!" title="Cita de Servicio">
												4425923862 
											</a>
											| 
											<a href="https://api.whatsapp.com/send?phone=524432718558&text=Hola,%20Quiero%20más%20información!" title="Cita de Servicio">
												4432718558 
											</a>
										</strong>
									</span>
								</li>

								<li><a href="#"><i class="fa fa-envelope"></i>contacto@hondamanantiales.com</a></li>
							</ul>
						</div>
					</div>

					<div class="col-md-3">
						<div class="contact-information">
							<h3>Horario de Atención</h3>
							<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>HONDA FAME Manantiales</strong>; te escuchamos y atendemos de manera personalizada. </p>
							<p class="work-time"><span>Lunes - Sábado</span> : 8:00 a.m. - 8:00 p.m.</p>
						</div>
					</div>
					
				</div>
			</div>
		</div>

	</div> 

	<br>
	
	<?php include('contenido/footer.php'); ?>
</div> 			

</body>
</html>