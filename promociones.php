<!doctype html>

<html lang="es" xml:lang="es" class="no-js">

<head>

	<title>Promociones</title>

	<?php include('contenido/head.php'); ?>

</head>

<body>

    <?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">

		<?php include('contenido/header.php'); ?>
        <?php include('contenido/analytics.php'); ?>

         <div class="page-banner">         
             <div class="container"> <h2><strong>Promociones</strong></h2> </div>
                 </div>

                 <div class="single-project-page">
                     <div class="container">
                         <div class="row">
<!-- PROMOCION ================================================= -->
                         <h2>HR-V</h2>
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Honda promociones " src="promos/HRV.jpg"></a>
                         </span>  

                         <div class="col-md-12">
                             <p align="justify"> 

                                Vigencia del 01 al 31 de marzo de 2019 o hasta agotar existencia lo que suceda primero. Mensualidades desde $3,185 en la compra de Honda® HR-V® en versión UNIQ MT modelo 2019, con un pago desde 50% (cincuenta) de enganche y plazos de 72 meses de $3,184.86 con un CAT de 31.6% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano. Tasa desde 11.99%. Vigencia del 01 al 31 de Marzo de 2019 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® HR-V® en versión UNIQ MT, UNIQ CVT, PRIME, EDICIÓN LIMITADA 1 MILLION Y TOURING modelo 2019, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 38.06% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.  

                             </p><br>
                         </div><br>
<!-- PROMOCION ================================================= -->

<!-- PROMOCION ================================================= -->
                         <h2>BR-V</h2>
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Honda promociones " src="promos/BRV.jpg"></a>
                         </span>  

                         <div class="col-md-12">
                             <p align="justify"> 

                                Vigencia del 01 al 31 de Marzo de 2019 o hasta agotar existencia lo que suceda primero. Bono de $12,000.00 aplica en la compra de Honda® BR-V® en versión UNIQ y PRIME modelo 2018. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano. Tasa desde 13.49%. Vigencia del 01 al 31 de Marzo de 2019 o hasta agotar existencia lo que suceda primero. Tasas desde 13.49% en la compra de Honda® BR-V® en versión UNIQ y PRIME modelo 2018 y PRIME modelo 2019, con un pago desde 30% de enganche y plazos desde 12 a 18 meses, con un CAT de 46.28% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano. 

                             </p><br>
                         </div><br>
<!-- PROMOCION ================================================= -->

<!-- PROMOCION ================================================= -->
                         <h2>CITY</h2>
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Honda promociones " src="promos/CITY.jpg"></a>
                         </span>  

                         <div class="col-md-12">
                             <p align="justify"> 

                                Vigencia del 01 al 28 de febrero de 2019 o hasta agotar existencia lo que suceda primero. Mensualidades desde 2,717 en la compra de Honda® City® en versión LX MT modelo 2019, con un pago desde 50% (cincuenta) de enganche y plazos de 72 meses de $2,716.18, con un CAT de 42.0% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.

                             </p><br>
                         </div><br>
<!-- PROMOCION ================================================= -->

<!-- PROMOCION ================================================= -->
                         <h2>CIVIC</h2>
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Honda promociones " src="promos/CIVIC.jpg"></a>
                         </span>  

                         <div class="col-md-12">
                             <p align="justify"> 

                                Vigencia del 01 al 31 de marzo de 2019 o hasta agotar existencia lo que suceda primero. Bono de $10,000.00 aplica en la compra de Honda® Civic® en versión EX I-STYLE CVT, COUPE CVT, TURBO PLUS CVT y TOURING CVT en modelos 2018. Tasa desde 11.99%. Vigencia del 01 al 31 de marzo de 2019 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® Civic® en versión I-STYLE CVT, COUPE CVT, TURBO PLUS CVT y TOURING CVT modelo 2018 y 2019, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 36.33% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano. 

                             </p><br>
                         </div><br>
<!-- PROMOCION ================================================= -->

<!-- PROMOCION ================================================= -->
                         <h2>CR-V</h2>
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Honda promociones " src="promos/CRV.jpg"></a>
                         </span>  

                         <div class="col-md-12">
                             <p align="justify"> 

                                Vigencia del 01 al 31 de Marzo de 2019 o hasta agotar existencia lo que suceda primero. Bono de $15,000.00 aplica en la compra de de Honda® CR-V® en versiones EX CVT, TURBO PLUS CVT, TOURING CVT modelo 2018. Tasa desde 11.99%. Vigencia del 01 al 31 de Marzo de 2019 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® CR-V® en versiones EX CVT, TURBO PLUS CVT, TOURING CVT modelo 2018 y 2019, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 33.83% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano. 

                             </p><br>
                         </div><br>
<!-- PROMOCION ================================================= -->

<!-- PROMOCION ================================================= -->
                         <h2>ODYSSEY</h2>
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Honda promociones " src="promos/ODYSSEY.jpg"></a>
                         </span>  

                         <div class="col-md-12">
                             <p align="justify"> 

                                Vigencia del 01 al 31 de Marzo de 2019 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® Odyssey® en versión Prime o Touring modelo 2019, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 26.97% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano. 

                             </p><br>
                         </div><br>
<!-- PROMOCION ================================================= -->

<!-- PROMOCION ================================================= -->
                         <h2>PILOT</h2>
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Honda promociones " src="promos/PILOT.jpg"></a>
                         </span>  

                         <div class="col-md-12">
                             <p align="justify"> 

                                Vigencia del 01 al 31 de Marzo de 2019 o hasta agotar existencia lo que suceda primero. Bono de $20,000.00 aplica en la compra de Honda® Pilot® en versión PRIME modelo 2019. Tasas desde 11.99%. Vigencia del 01 al 31 de Marzo de 2019 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® Pilot® en versión Prime o Touring en modelo 2019, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 29.88% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano. 

                             </p><br>
                         </div><br>
<!-- PROMOCION ================================================= -->

<!-- PROMOCION ================================================= -->
                         <h2>TYPE-R</h2>
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Honda promociones " src="promos/TYPE-R.jpg"></a>
                         </span>  

                         <div class="col-md-12">
                             <p align="justify"> 

                                Vigencia del 01 al 31 de Marzo de 2019 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® Type-R® modelo 2017 y 2018, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 33.96% sin IVA promedio informativo para modelo 2018 y CAT de 35.84% sin IVA promedio informativo para modelo 2017, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano. 

                             </p><br>
                         </div><br>
<!-- PROMOCION ================================================= -->


<!-- PROMO 1 ================================================= 

                         <span class="single-project-content">
                         </span>                        
                         <div class="col-md-12">

				      	     <h2>BR-V</h2>

				      	     <p align="justify"> 

				      	     	Vigencia del 01 al 31 de octubre de 2018 o hasta agotar existencia lo que suceda primero. Mensualidades desde $3,293 en la compra de Honda® BR-V® en versión UNIQ modelo 2018, con un pago desde 50% (cincuenta) de enganche y plazos de 72 meses de $3,292.33 sin seguro incluido. CAT de 33.19 % sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. 0% de comisión por apertura en la compra de Honda® BR-V® en versión UNIQ y PRIME modelo 2018, con un pago desde 10% (diez) de enganche y plazos de 12 a 72, con un CAT de 33.75% sin IVA promedio informativo para modelo 2018, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.

				      	     </p><br>

	                     </div><br><br>      
--> 
 

     </div>
     </div>
     </div>                      

<<?php include('contenido/footer.php'); ?>

                     </div>

                 </div>

             </div>



 </body>

</html>