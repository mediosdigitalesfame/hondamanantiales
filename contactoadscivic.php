<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Contacto</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
		<?php include('contenido/header.php'); ?>
		<?php include('contenido/analytics.php'); ?>
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Contáctanos</h2>

				</div>
			</div>

			<div class="contact-box">
				<div class="container">
					<div class="row">
						
						<div class="col-md-12" align="center">

							 
						<div class="container">
							<div class="col-md-12" >
								<?php include('jotform.php'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div> 

	<br>
	
	<?php include('contenido/footer.php'); ?>
</div> 			

</body>
</html>